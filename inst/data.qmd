---
engine: knitr
---

# Data

```{r}
#| echo: false
#| output: false

devtools::load_all()
```

The recommended place to stora raw data was `inst/extdata` but, now, the practice is to store raw data at `data-raw`.
And the recommended place to store clean data, as `*.rda`, is `data`.
The data cleaning steps should be recorded in `data-raw/DATASET.R`, for example

```{bash}
#| echo: false

cat ../data-raw/DATASET.R
```

It is common to **manually** execute `data-raw/DATASET.R` by running

```{r}
#| eval: false

source("data-raw/DATASET.R")
```

One way to automate the execution of `data-raw/DATASET.R` is to use Quarto's `pre-render` in `_quarto.yml`:

    project:
      type: book
      pre-render:
        - '../tools/build-data.sh'

Quarto executes the `pre-render` steps only once for `preview`.
