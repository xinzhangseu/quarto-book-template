#!/bin/bash
#
# After run
#
#     $ docker-compose up --force-recreate --build
DOCKER_IMAGE="registry.gitlab.com/raniere-phd/quarto-document-template"
docker image tag quarto-document-template_web $DOCKER_IMAGE
docker push $DOCKER_IMAGE
